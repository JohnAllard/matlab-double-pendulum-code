x = -10:.2:10;
y = -10:.2:10;
q = 2;
[X,Y] = meshgrid(x,y);
t = 1:.05:10;
for t = 1:100
    
    r = sqrt((X - sqrt(t) + 2).^2 + (Y+sqrt(t) + 2).^2);
    r2 = sqrt((X + sqrt(t) + 2).^2 + (Y - sqrt(t) + 2).^2);
    z = 1.*q./(r+.6) + 1.*q./(r2+.6);
    surf(X,Y,z); zlim([0 4]);
    
    pause(.1);

end