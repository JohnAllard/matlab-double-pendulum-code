

% This solves and animates the motion, angle, or angular velocity of a double 
% pendulum system as it progresses through time
%
%   @author John Allard
%   ---------------------------------------------------------------------
%   
%   @params ic1
%   ic = [theta1u; angvel1u; theta1l; angvel1l; grav; mass1u; mass1l; len1u; len1l]
%   ic - Initial Conditions, is an 9 length row vector that represents the
%   initial conditions one double pendulum system
%   'u' after the parameter name means upper, 'l' means lower.
%
%   @param settings1
%   settings1 = [plottype; colors1.. colors3; othersettings1...3]
%   the first param is the plottype, this param determines how many other
%   params will be inside this vector. Depending on the plot type, the next
%   1 to 3 indexes with be the colors for the plot, depending on the plot
%   type the next 1 through 3 indices will be the other plot settings like
%   the linewidth or angle plot type.
%
%
%   @params ic2
%   ic = [theta2u; angvel2u; theta2l; angvel2l; grav; mass2u; mass2l; len2u; len2l]
%   ic - Initial Conditions, is an 9 length row vector that represents the
%   initial conditions one double pendulum system
%   'u' after the parameter name means upper, 'l' means lower.
%
%   @param settings2
%   settings2 = [plottype; colors1.. colors3; othersettings1...3]
%   the first param is the plottype, this param determines how many other
%   params will be inside this vector. Depending on the plot type, the next
%   1 to 3 indexes with be the colors for the plot, depending on the plot
%   type the next 1 through 3 indices will be the other plot settings like
%   the linewidth or angle plot type.
%
%   @param simoptions
%   simoptions = [simtime; simspeed;
%   simtime is he length in seconds of the simulation. It must always start at zero,
%   and it's value will be the length of the simulation at 100% of normal
%   running speed
%   simspeed is a double that represents a percentage in a decimal
%   format. This percentage is the simulation speed as a percentage of the
%   normal speed. So 50% speed would have simspeed = 0.5.
%
%   ---------------------------------------------------------------------

function physim_DP_simulation(ic1, settings1, ic2, settings2, simoptions)
clear All;

time = simoptions(1);
simspeed = simoptions(2);

fpsnormal = 50;
fps = fpsnormal*simspeed; 
numframes=time*fps;
 
options = odeset('Refine',6,'RelTol',1e-5,'AbsTol',1e-7); 
t = linspace(0,time,numframes);

solutionsstruct1=ode45(@DoublePendEquations,[0 time], ic1, options);
solutionsvector1=deval(solutionsstruct1,t);

solutionsstruct2=ode45(@DoublePendEquations,[0 time], ic2, options);
solutionsvector2=deval(solutionsstruct2,t);

% get the individual components of the solution vector1
theta1u=solutionsvector1(1,:)';
angvel1u=solutionsvector1(2,:)';
theta1l=solutionsvector1(3,:)';
angvel1l=solutionsvector1(4,:)';
len1u=ic1(8); len1l=ic1(9);
m1u = ic1(6); m1l = ic1(7);
moment1u = 0.5.*m1u.*len1u.^2;
moment1l = 0.5*m1l.*len1l.^2;
grav1 = ic1(5);

% get the individual components of the solution vecto2
theta2u=solutionsvector2(1,:)';
angvel2u=solutionsvector2(2,:)';
theta2l=solutionsvector2(3,:)'; 
angvel2l=solutionsvector2(4,:)';
len1u=ic2(8); len2l=ic2(9);
m2u = ic2(6); m2l = ic2(7);
moment2u = 0.5.*m2u.*len2u.^2;
moment2l = 0.5*m2l.*len2l.^2;
grav2 = ic2(5);


maxval= max(angvel2u(:));
maxval2 = max(angvel2l(:));




%create the figure window, set it to outer edges of screen
figure('units','normalized','outerposition',[0 0 1 1]);


%--- PLOT 1 configured here ----
% this subplot defnes the coordinates and size of the pendulum pos. plot
subplot('Position',[.03 .1 .52 .8]);hold on;

% PENDULUM POSITION PLOT
if settings1(1) == 1
    
    % plot pendulum rods
    plot11 = plot(0,0,'k', 'LineWidth',2); 
    
    %this plot places the objects on the ends up the pendulums
    ColorSet = [0 0 0; settings1(2);settings1(3)];
    plot13 = scatter([0 0 0], [0 0 0], [50, 100, 100], ColorSet, 'filled');
    axis equal;
    
    % this plots the line that shows where the bottom pendulum has been
    if(settings1(5) == 1) %include trailing line
        plot12 = plot([0 0], [0 0], 'Color', settings1(4));
    end
    
    if(settings1(6) == 1) %include grid
        grid on;
    end
    if(settings1(7) == 1) %include title
        title(' Position Animation ');
    end
    hold off;
    range=1.1*(len1u+len1l); axis([-range range -range range]); %pos plot axis limits
end

% ENERGY BAR GRAPH
%plot type, kin color, pot color, total color, inc kin, inc pot, inc total,
%grid , titile
if settings1(1) == 2
    plot11 = plot(0,0,'k', 'LineWidth',2); %the pendulum rod
    % this plots the line that shows where the bottom pendulum has been
    plot12 = plot([0 0], [0 0], 'Color', [1 153/255 0]);
    %this plot places the objects on the ends up the pendulums 
    ColorSet = [0 0 0; settings1(2);settings1(3)];
    plot13 = scatter([0 0 0], [0 0 0], [50, 100, 100], ColorSet, 'filled');
    axis equal;
    range=1.1*(len1u+len1l); axis([-range range -range range]); %pos plot axis limits
    hold off;
end

% PENDULUM ANGLE PLOT
% plot type, line color, line thickness, grid, title
if settings1(1) == 3
    
    % plot pendulum rods
    plot11 = plot([0 0],[0 0],'Color',settings2(2), 'LineWidth',settings2(3)); 
    
    if(settings1(4) == 1) %include grid
        grid on;
    end
    if(settings1(5) == 1) %include title
        title(' Position Animation ');
    end
    hold off;
    range=1.1*(len1u+len1l); axis([-range range -range range]); %pos plot axis limits
end







%--- PLOT 2 configured here ----
subplot('Position', [.56 .1 .38 .8]);hold on;
if settings2(1) == 1
% plot pendulum rods
    plot11 = plot(0,0,'k', 'LineWidth',2); 
    
    %this plot places the objects on the ends up the pendulums
    ColorSet = [0 0 0; settings1(2);settings1(3)];
    plot13 = scatter([0 0 0], [0 0 0], [50, 100, 100], ColorSet, 'filled');
    axis equal;
    
    % this plots the line that shows where the bottom pendulum has been
    if(settings1(5) == 1) %include trailing line
        plot12 = plot([0 0], [0 0], 'Color', settings1(4));
    end
    
    if(settings1(6) == 1) %include grid
        grid on;
    end
    if(settings1(7) == 1) %include title
        title(' Position Animation ');
    end
    hold off;
    range=1.1*(len1u+len1l); axis([-range range -range range]); %pos plot axis limits
end



