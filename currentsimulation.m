function currentsimulation(ic)

options = odeset('Refine',6,'RelTol',1e-5,'AbsTol',1e-7);
solutionsstruct = ode45(@TEST, [0 40], ic, options);
t = linspace(0,40, 8000);
solutionsvector=deval(solutionsstruct,t);

time = 1:8000;
time = time*40/8000;

x = solutionsvector(1,:);
figure('units','normalized','outerposition',[0 0 1 1]);
plot(time, x); xlim([0 .5]);title('Charge on capacitor against time'); grid on;


figure('units','normalized','outerposition',[0 0 1 1]);
I2 = ic(2)/ic(3) - x/(ic(5)*ic(3)) - x/(ic(5)*ic(4));
I1 = ic(2)/ic(3) - x/(ic(5)*ic(3));
I3 = I1 - I2; 

plot(time, I1, 'r', time, I2, 'b', time, I3, 'g', 'LineWidth', 1.5); xlim([0 .5]);grid on;
title('I1, I2, and I3 against time','FontWeight', 'bold');legend('I1 = red','I2 = blue', 'I3 = green');
