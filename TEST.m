function xdot = TEST(t, ic)

Q = ic(1);
E = ic(2);
R1 = ic(3);
R2 = ic(4);
C = ic(5);
xdot = zeros(5, 1);

xdot(1) = E/R1 - Q/(C*R1) - Q/(C*R2);

